---
id: runsandbox
title: Start the sandbox for the first time
sidebar_label: Run Sandbox
---
This sandbox is meant to be the easiest way to get started with the SynchroniCity Framework. It is based on a microservice architecturte where you can mix'n'match the components you need to solve your specific problems. In this turorial we will focus on two components:

* Baseline NGSI Context Broker
* Long Term Historical NGSI

## Start the overlay network
We rely on an external docker overlay network so all the components in the microservice architecture can communicate with eachother. This must be created first using the following command.
```
docker network create main
```
this is a prerequisite for the rest of the tutorial.

## Baseline NGSI Context Broker
Starting this service will spin up a MongoDB v3.6 database and link it to the Orion Context Broker. There is no configuration required for the sandbox.

```
docker-compose -f baseline-ngsi/compose-NGSI.yml up -d
```
This will start an instance of the services in the background, and output the following to the screen:
```
Creating mongo-cb ... done
Creating orion-cb ... done
```
You can check that the services are running with
```
docker-compose -f baseline-ngsi/compose-NGSI.yml ps
```
You should see the following:
```
  Name                Command               State            Ports
----------------------------------------------------------------------------
mongo-cb   docker-entrypoint.sh mongo ...   Up      0.0.0.0:27017->27017/tcp
orion-cb   /usr/bin/contextBroker -fg ...   Up      0.0.0.0:1026->1026/tcp
```
If the state is not `Up`, rerun the `docker-compose -f baseline-ngsi/compose-NGSI.yml up -d` command.

### Testing the NGSI Context Broker
The service will run instantly and you can check the status either through the terminal or through your browser at this address: [http://localhost:1026/v2](http://localhost:1026/v2).

Using cURL you neet to enter the following:
```
curl http://localhost:1026/version
```

The output JSON will look like this:
```json
{
  "orion" : {
    "version" : "2.0.0-next",
    "uptime" : "0 d, 0 h, 0 m, 28 s",
    "git_hash" : "81eeaf3ce4c99191a1830479eddf9e9e35c55029",
    "compile_time" : "Tue Nov 27 11:31:55 UTC 2018",
    "compiled_by" : "root",
    "compiled_in" : "e7d2397ebcc5",
    "release_date" : "Tue Nov 27 11:31:55 UTC 2018",
    "doc" : "https://fiware-orion.rtfd.io/"
  }
}
```
Congratulations. You are now ready to POST your first NGSI data to the Context Broker.

### Storing data in the Context Broker
To test the storage of data in the Context Broker, copy the cURL command below into your terminal. It will store an new entity of type `AirQualityObserved`.

```bash
curl -X "POST" "http://localhost:1026/v2/entities?options=keyValues" \
  -H 'Content-Type:application/json' \
  -d $'{
    "id": "vehicle:WasteManagement:black-box",
    "type": "Vehicle",
    "category": {
      "value": [
        "municipalServices"
      ]
    },
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [
          56.18786,
          10.16818
        ]
      },
      "metadata": {
        "timestamp": {
          "type": "DateTime",
          "value": "2019-01-21T06:36:34.766099026Z"
        }
      }
    },
    "name": {
      "value": "vehicle:WasteManagement:black-box"
    },
    "refVehicleModel": {
      "type": "Relationship",
      "value": "vehiclemodel:econic"
    },
    "serviceProvided": {
      "value": [
        "garbageCollection",
        "wasteContainerCleaning"
      ]
    },
    "vehicleType": {
      "value": "lorry"
    }
  }'
```

To test if your data was stored correctly, use the cURL command below to GET your entity back.

```bash
curl "http://localhost:1026/v2/entities/vehicle:WasteManagement:black-box?options=keyValues" \
     -H 'Accept: application/json'
```

The output will be a JSON looking like this:
```json
{
  "id": "vehicle:WasteManagement:black-box",
  "type": "Vehicle",
  "category": {
    "value": [
      "municipalServices"
    ]
  },
  "location": {
    "type": "geo:json",
    "value": {
      "type": "Point",
      "coordinates": [
        56.18786,
        10.16818
      ]
    },
    "metadata": {
      "timestamp": {
        "type": "DateTime",
        "value": "2019-01-21T06:36:34.766099026Z"
      }
    }
  },
  "name": {
    "value": "vehicle:WasteManagement:black-box"
  },
  "refVehicleModel": {
    "type": "Relationship",
    "value": "vehiclemodel:econic"
  },
  "serviceProvided": {
    "value": [
      "garbageCollection",
      "wasteContainerCleaning"
    ]
  },
  "vehicleType": {
    "value": "lorry"
  }
}
```
Congratulations, you have successfully created a new entity, and stored a series of attributes in the Context Broker.

> Note: It is *very* important that all calls have the URL parameter `?options=keyValues` for these examples to work properly.

## Long Term Historical NGSI
The long term historical service is slightly more involved. It will start its own MongoDB service, then the Cygnus service and finally the API serbservice. There is no configuration required for the sandbox, but additional options can be added later to the `.conf`and `.js`configuration files.

```
docker-compose -f historical-ngsi/compose-historical.yml up -d
```
This will start an instance of the services in the background, and output the following to the screen:
```
Creating mongo-his      ... done
Creating historical-api ... done
Creating cygnus-ngsi    ... done
```
You can check that the services are running with
```
docker-compose -f historical-ngsi/compose-historical.yml ps
```
You should see the following:
```
     Name                   Command               State                         Ports
------------------------------------------------------------------------------------------------------------
cygnus-ngsi      /cygnus-entrypoint.sh            Up      0.0.0.0:5051->5050/tcp, 0.0.0.0:8082->5080/tcp
historical-api   pm2-runtime start pm2.json       Up      43554/tcp, 443/tcp, 80/tcp, 0.0.0.0:8080->8080/tcp
mongo-his        docker-entrypoint.sh mongo ...   Up      0.0.0.0:27117->27017/tcp
```
If the state is not `Up`, rerun the `docker-compose -f historical-ngsi/compose-historical.yml up -d` command.

### Testing the Long Term Historical NGSI
The service will take a while to fully start but once completed you can check the status either through the terminal or through your browser at this address: [http://localhost:8082/v1/version](http://localhost:8082/v1/version).

Using cURL you need to enter the following:
```
curl http://localhost:8082/v1/stats
```

The output JSON will look something like this:
```json
{
  "success": "true",
  "stats": {
    "sources": [
      {
        "name": "ORION-source",
        "status": "START",
        "setup_time": "2019-01-21T12:15:59.437Z",
        "num_received_events": 0,
        "num_processed_events": 0
      }
    ],
    "channels": [
      {
        "name": "mongo-channel",
        "status": "START",
        "setup_time": "2019-01-21T12:15:59.546Z",
        "num_events": 0,
        "num_puts_ok": 0,
        "num_puts_failed": 0,
        "num_takes_ok": 0,
        "num_takes_failed": 3
      }
    ],
    "sinks": [
      {
        "name": "SYNC-historical",
        "status": "START",
        "setup_time": "2019-01-21T12:15:59.474Z",
        "num_processed_events": 0,
        "num_persisted_events": 0
      }
    ]
  }
}
```
Congratulations.
