---
id: datamodels
title: Understanding Datamodels
sidebar_label: Understanding Datamodels
---
This sandbox is meant to be the easiest way to get started with the SynchroniCity Framework. It helps you download and install the latest version, with examples that demonstrate how powerful it can be. 