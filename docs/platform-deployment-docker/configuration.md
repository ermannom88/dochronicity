---
id: configuration
title: Installation and configuration
---

## Running with Docker compose

The whole architecture prototype will run as a set of containers, created and linked through the use of the docker-compose functionality, as defined in the provided [`docker-compose.yml`](docker-compose.yml) file. For deploying the SynchroniCity platform execute the following steps.

1. **Clone the platform repository with:**

```
git clone https://gitlab.com/synchronicity-iot/platform-deployment-docker.git
```
2. **Go inside the created folder (where it is the `docker-compose.yml`):**

```
cd platform-deployment-docker
```
3. **Perform all the modification to the provided configuration files, and if needed to the docker-compose.yml, as described in the [Configuration](#def-configuration) section below.**

4. **ONLY AFTER COMPLETING STEP 3** - Run the docker-compose file with:
```
docker-compose up
```
The containers will be automatically started and attached to the network specified in the docker-compose (*main_synchronicity*).
***

<a name="def-configuration"></a>
## Configuration


**Pay attention**
**All the modifications to the configuration files require restarting the relative container.
**DO NOT USE** **`docker-compose down`** command, it will DESTROY all the containers of the architecture. Use instead either **`docker-compose restart`** or **`docker container_name restart`**

**Reference:** [https://docs.docker.com/compose](https://docs.docker.com/compose/)
The platform configuration, consists of two steps:
 1. Configuring components to communicate with its related Mongo persistence container.
 2. Modifying component specific configurations.

### 1. Components and related Mongo containers

As depicted in the architecture figure, each component has its related persistence container (iot-mongo -> orion , mongo-iota-manager -> iota-manager, etc).
***
#### **a. Configuring Mongo containers**

Host and port where each Mongo Container will be reachable is specified, in the relative part of the contianer, by:
- **Host**: the name of the container itself (iot-mongo, iota-manager, etc.)
- **Port**:
    - the first part of its `ports` section (e.g. **`27017`**`:27017`)
    - the **"--port"** command line parameter in `command` section (e.g. `--port 27017`).
* In case of authenticated Mongo, set following variables in `environment` section
    * `MONGO_INITDB_ROOT_USERNAME` and `MONGO_INITDB_ROOT_PASSWORD` (e.g. root, root)
     and set, accordingly to these chosen values, the relative command line parameters in the `command` part of the containerthat uses it.

**Example**:
```
    iot-mongo:
       image: mongo:3.4
       environment:
         - MONGO_INITDB_ROOT_USERNAME=root
         - MONGO_INITDB_ROOT_PASSWORD=root
       command: mongod --nojournal --bind_ip 0.0.0.0 --port 27017
       ports:
        - "27017:27017"
       volumes:
        - './mongo_data:/data/db'
```

#### Important note

If the authentication is enabled, the specified user, **MUST** be a superuser in MongoDB, in order to be able to perform all the CRUD operations in **ANY** DB, which are created one per tenant. In detail, it should have the following roles:
* readWriteAnyDatabase
* dbAdminAnyDatabase
* clusterAdmin

***
#### **b. Configuring components to use Mongo container**
The container of components that use a Mongo container, must be configured to match the configuration specified previously, namely the host and port where to reach Mongo.
In detail, depending on the particular component, they must be specified:
- as environment variables in the `environment` section
- as command line parameters in the `command` section.

#####  Configuring mongo connnection for Orion Context Broker
The "**orion**" container uses "**iot-mongo**" persistence container.
Add to its `command` section the following parameter:
 - `command: -dbhost iot-mongo`

If that Mongo container has been setup with authentication, add two additional command line parameters in `command` section:
 - `command: -dbhost iot-mongo -dbuser root -dbpwd root`
Change 'root' values with the ones specified when configuring the Mongo container authentication, as described in the previous section.

##### Configuring mongo connection for other components
All the oher components will manage configuration for mongo connection through environment variables, specified in their own `environment` section.
Following sections will specify the environment variables that must be set, for each specific component.
***

### 2. Component specific configurations
The configuration files that will be modified, are provided in the root folder of this [repo](https://gitlab.com/synchronicity-iot/platform-deployment-docker) and they will be mounted as volume when the docker compose will start.


### Cygnus Connector and sinks
The Cygnus container will mount a volume, in order to install in the container the provided **`agent.conf`** configuration file.
#### Configuring sinks
This file is already configured to setup only the **mongo-sink** and **sth-sink**, which are used by the architecture in the current version.
These sinks have the `mongo_hosts` parameter pointing to the container hostname where is deployed the mongo instance used by the **cygnus** container, namely `mongo-cygnus:27018`**

In order to correctly configure the mongo and sth sinks for Cygnus, modify, if needed the following properties.

- **`cygnus-ngsi.sinks.mongo-sink.mongo_hosts = host_and_port_where_is_mongo`**

If Mongo container authentication is enabled:
- **`cygnus-ngsi.sinks.mongo-sink.mongo_username = username (if Mongo authentication is enabled)`**

- **`cygnus-ngsi.sinks.mongo-sink.mongo_password = password *(if Mongo authentication is enabled)`**

Currently, the authentication is disabled by default.

**ImportantNote.**
When the container is restarted, its entrypoint script overwrites previous parameters in the **agent.conf** file, with the ones set in the following environment variables:

* `CYGNUS_MONGO_USER=root` **(if Mongo authentication is enabled, change with actual values)**

* `CYGNUS_MONGO_PASS=root` **(if Mongo authentication is enabled, change with actual values)**

* `CYGNUS_MONGO_HOSTS=mongo-cygnus:27018`

Thus, in order to avoid this replacement for each restart, instead of editing the agent.conf, these variables should be modified in the `environment` section of Cygnus container.

* * *
### STH-Comet
The "**sth-comet**" container uses **mongo-cygnus**, then modify the following environment variables:
 - `DB_URI=mongo-cygnus:27018`

### IoT Agent Manager
#### Mongo configuration
The "**iota-manager**" container uses **mongo-iota-manager**, then modify the following environment variables:
*   `IOTA_MONGO_HOST= mongo-iota-manager`
*   `IOTA_MONGO_PORT= 27019`

In **mongo-iota-manager** the `ports` section has been setup with:  
	 * "**27019**:27017"
The Mongo container will be available (externally to the container itseltf), at port **27019**. For this reason we set accordingly the environment variables above.

#### Subscription configuration
The hostname of agents are used when specifying the agent URL during its subscription to the agent manager.
For instance, the "**iotagent**" field would be **http://iota-ul:4041** for subscription of the IoT Agent Ultralight.

**For further details**
[ https://github.com/telefonicaid/iotagent-manager#-subscription-api](https://github.com/telefonicaid/iotagent-manager#-subscription-api)).
