---
id: examples
title: Usage Scenarios examples and annotations
---

## Orion context Broker Multiservices

The Orion Context Broker implements a simple multitenant / multiservice model based and logical database separation. Multitenant / multiservice ensures that the entities/attributes/subscriptions of one service/tenant are "invisible" to other services/tentants.

This functionality is activated in the SynchroniCity prototype, Orion uses the "Fiware-Service" and (optional for subpath) "Fiware-ServicePath" HTTP headers in the request to identify the service/tenant. If the headers are not present in the HTTP request, the default service/tenant is used.

### **Example**

For example, queryContext on tenantRZ1 space will never return entities/attributes from tenantRZ2 space.

* Create a device with Fiware-Service "**tenantRZ1**" and id “**device1**”.

![image alt text](assets/platform-deployment-docker/image_3.png)

* Get the newly created device in this service/tenant, note the value "**10.0**" for the **temperature** attribute.

![image alt text](assets/platform-deployment-docker/image_4.png)

* Create a device with Fiware-Service "**tenantRZ2**" and id “**device1**”.

![image alt text](assets/platform-deployment-docker/image_5.png)

![image alt text](assets/platform-deployment-docker/image_6.png)

* Get the newly created device in this service/tenant, note the value "**20.0**" for the **temperature** attribute. Despite the id is the same of the previous one (**device1**), the temperature has another value, because we are in a different tenant (**tenantRZ2**) and indeed, this is another entity.

* * *


## Scenario 1 (COAP w/o IoTA Manager)

![image alt text](assets/platform-deployment-docker/image_7.png)

## Scenario 2 (with IoTA Manager)

The following Scenario describes the interaction of the IoT-Agent Manager that acts as proxy for the provisioning of the IoT Agents, related devices and services/tenants.

As described in [https://github.com/telefonicaid/iotagent-manager#-subscription-api](https://github.com/telefonicaid/iotagent-manager#-subscription-api) :

1. Register IoT Agent (iota-ul) to IoTA Manager (iota-manager) with service (tenant):

	* Fiware-Service and ServicePath: **myhome**, **/environment**

	* Protocol: **IoTA-UltraLight**

	* Resource: **/iot/d**

	* IotAgent: [http://iota-ul:4041](http://iota-ul:4041) (**iota-ul**, as like the following ones contained in any URL fields, is the hostname visible to the IotA Manager, due to the Docker linkage process, as described in the "Docker compose configuration" section).

![image alt text](assets/platform-deployment-docker/image_8.png)

* * *


2. Create a new service associated with a tenant (**myhome/environment**) and a protocol (the agent protocol previously created, e.g. **IoTA-UltraLight**). In this case, as shown in the following body request, the "**service**" to be created is considered as a common configuration for all the devices belonging to this tenant. This request is forwarded to the agent specified in the “**protocol**" query parameter. Indeed, the agent provides the same API, but in this case the “service” is created both in the IoTA-Manager and then in the registered IoT Agent.

![image alt text](assets/platform-deployment-docker/image_9.png)

In this example, after the "service" creation, all the devices having the same **apikey-resource** pair value, will have the same base configuration. For example, all these devices will have the **status** active attribute.

***

3. Register a device to the registered IoT Agent through IoTA Manager by specifying the agent protocol in the query parameter (**IoTA-UltraLight**), the tenant headers (Fiware-Service and ServicePath: **myhome**, **/environment**), in addition to the following fields relative to the device and the resulting mapped entity:

	* Device_id: **sensor01**

	* Entity name and Type: **LivingRoomSensor**, **multiSensor**

	* Attributes:  object Id --> name, type (mapping between device and entity attributes)

![image alt text](assets/platform-deployment-docker/image_10.png)

As shown below, the device was registered to its Agent. The attributes array contains the mapping between a device attribute (**object_id**) and the resulting entity attribute (**name-type** pair).

![image alt text](assets/platform-deployment-docker/image_11.png)

* * *


4. The IoT Agent creates the mapped entity, by issuing a NGSI request to the Context Broker, corresponding to its registered device. Even in this case, this API could be invoked directly against the Agent, but in this way the IoTA Manager is used as a proxy and single provisioning point.

	As shown below, by querying the Context Broker for the entities belonging to the (**myhome/environment**) tenant, we get the mapped entity **"LivingRoomSensor"**.

![image alt text](assets/platform-deployment-docker/image_12.png)

* * *


5. The registered device sends a measure via HTTP (in case of active attribute) 		to the IoT UltraLight Agent, which translates the payload to a NGSI request and sends it to the ContextBroker. As shown below, we simulate a device sending measurements, through the HTTP southbound endpoint exposed by IoTAgent.

![image alt text](assets/platform-deployment-docker/image_13.png)

In this case, we send the value **29.5** for the attribute **t** of the device with id **sensor01** and apikey **ABCDEF**.

* * *


6. The attribute value (**temperature**) of the mapped entity is updated in the Context Broker with the value (**29.5**).

![image alt text](assets/platform-deployment-docker/image_14.png)

* * *


7. If there is a subscription with the appropriate endpoint, Cygnus through its configured sinks (**mongo-sink** and **sth-sink**) stores historical raw and aggregated context information in the mongo container (**mongo-cygnus**).
* Create the subscription in the Orion Context Broker for the mapped entity. Note the **reference** field, it points to the Cygnus endpoint .

![image alt text](assets/platform-deployment-docker/image_15.png)

- Check in the **mongo-cygnus** instance with a Mongo Client (e.g. **Robo 3T**) that the following have been created:

* **mongo_myhome** DB for historical raw data with the 	**mongo_/environment_LivingRoomSensor_multiSensor** collection. It contains the entry with the attribute (**temperature - 29.5**) that was just updated.

![image alt text](assets/platform-deployment-docker/image_16.png)

* **sth_myhome** DB for historical aggregated data with the **sth_/environment_LivingRoomSensor_multiSensor.aggr** collection. It contains a set of documents representing points calculated for the aggregated historical views.

![image alt text](assets/platform-deployment-docker/image_17.png)

* * *


8.  Retrieve historical raw context information through REST API exposed by the STH component. In particular we get the last **3** values for the **Temperature** attribute of **LivingRoomSensor** entity.

![image alt text](assets/platform-deployment-docker/image_18.png)

* * *


9. Retrieve historical aggregated context information though REST API exposed by the STH component. In particular we get the **minimum** value of the **Temperature** attribute, with a time resolution of a **day** (the minimum temperature value in a day).

![image alt text](assets/platform-deployment-docker/image_19.png)
