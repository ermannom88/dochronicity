---
id: security
title: Security Configuration
---

As previously described, the PEP-Proxy will intercepts all the requests coming to the entrypoint of the architecture. This endpoint is represented by a Nginx Reverse Proxy that, without any security components, would redirect all the requests to the different internal endpoints corresponding to the different containers. For instance, a request to "[https://services.synchronicity-iot.eu/api/context](https://services.synchronicity-iot.eu/api/context)" coming to the 80 or 443 port of the machine hosting Docker (and then to Nginx), would be redirected (internally) to localhost:1026.

![image alt text](assets/platform-deployment-docker/image_2.png)

The figure describes the solution provided, where was introduced the Pep-Wilma:

1. A registered user or an application registered in the user account of the IdM, sends a request with an authentication token (provided by Oauth APIs of the IdM), to the entrypoints of the architecture, as reported in  [Summary table of endpoints](#heading=h.17dp8vu)).

2. The request for https://services.synchronicity-iot.eu is redirected to the IdM, listening to the internal port 8000.

1. The requests to be protected, namely all the services.synchronicity-iot.eu/api/* (except security), are redirected to PEP-Wilma, which listens to port 81 or 444.

1. Pep Proxy validates the token against the IdM, if valid, forwards the original request to the port :8080 of proxy; otherwise, responds back with an authentication error.

2. Nginx redirects the granted request coming from Pep Proxy, to the appropriate container port, according to the host name present in the original request.

For further details, see the provided Nginx configuration file (default) and the documentation for Nginx as reverse proxy ([here](https://www.nginx.com/resources/admin-guide/reverse-proxy/)). For configuring NGINX in HTTPS, please consider to use CertBot (documentation [here](https://certbot.eff.org/all-instructions/#ubuntu-16-04-xenial-nginx)), which automatically generates and installs certificates both in the machine running Nginx and in its configuration file (located in **/etc/nginx/site-available/default**).

 ***
**Pay attention**
**All the modifications to the configuration files require restarting the relative container.
**DO NOT USE** **`docker-compose down`** command, it will DESTROY all the containers of the architecture. Use instead either **`docker-compose restart`** or **`docker container_name restart`**
### Idm Configuration
***
The IdM container will mount a volume, in order to install in the container the provided `local_settings.py` configuration file. This file must be modified, either before launching “docker-compose up” and then mounting it in the container or after the container startup, by entering in the container itself and modifying the file in the `“/horizon/openstack_dashboard/local/local_settings.py'` path.

The following configurations should be modified (see [here](http://fiware-idm.readthedocs.io/en/latest/setup.html) for other recommended production setups).

**Email**

By default Keyrock prints the registration confirmation email in console, this behavior **MUST** be avoided, in order to receive a real mail and to active a newly created account. It is needed to install a SMTP server, either in the IDM container or in the host machine: the typical choice is POSTFIX (see [here ](https://help.ubuntu.com/lts/serverguide/postfix.html)for installation).

In the `local_settings.py` modify the following variables:

- **`EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'`**
- Change **`EMAIL_HOST`** and **`EMAIL_PORT`** parameters according to the host where is reachable the installed SMTP server (e.g. POSTFIX).
	* **`EMAIL_HOST = '172.18.0.1'`**
	* **`EMAIL_PORT = 25`**

**Note** .In our dockerized environment, we could install the SMTP server in the host machine, thus we have to use as EMAIL_HOST the IP of the docker network gateway.

- Change the `EMAIL_URL` parameter with the one where the IdM is reacheable (in the prototype case in the Nginx entrypoint).

	*	**`EMAIL_URL = 'https://synchronicity-iot.eu'`**

- Change the following self-explaining parameters:
	* **`DEFAULT_FROM_EMAIL = 'your-no-reply-address'`**
	* **`EMAIL_SUBJECT_PREFIX = '[Prefix for emails subject]'`**

1. If the SMTP server requires authentication (not needed in the prototype, as POSTFIX is reachable on port 25 only internally).
	* **`EMAIL_HOST_USER = 'username'`**
	* **`EMAIL_HOST_PASSWORD ='password'`**

### **Pep-Proxy Wilma Configuration**
We are two options for using the PEP Proxy Wilma components:
1) If you want to use the original Fiware version of the PeP proxy, read the following section.
2) If instead, you want to use the "Plus" version, please see the installation documentation [(here)](https://gitlab.com/synchronicity-iot/fiware-pep-proxy)

The Pep-Proxy will mount a volume, in order to install in the container the provided **`config.js`** configuration file. This file should be modified, either before launching “docker-compose up” and then mounting it in the container or after the container startup, by entering in the container itself and modifying the file in the **“/opt/fiware-pep-proxy/config.js'** path.

#### Fiware Pep-Proxy Configuration
Edit the `config.js` file mounted as volume in the PEP Proxy container, by modifying the following relevant part:
```
// Used only if https is disabled
config.pep_port = 81;

// Set this var to undefined if you don't want the server to listen on HTTPS
config.https = {
    enabled: false,
    cert_file: '/etc/ssl/certs/pep-selfsigned.crt',
    key_file: '/etc/ssl/certs/pep-selfsigned.key',
    port: 444
};

config.account_host = 'http://localhost:8000';

config.keystone_host = 'localhost';
config.keystone_port = 8001;

config.app_host = 'localhost';
config.app_port = '8080';
// Use true if the app server listens in https
config.app_ssl = true;

// Credentials obtained when registering PEP Proxy in Account Portal
config.username = 'pep_proxy_9c5c15315a1f48bb8a0de6dbf46f7a8d';
config.password = 'c3049fc7903544e3a81077530391b2b3';
```

* **`config.pep_port`**: where the PeP-Proxy will listen to (in HTTP)

* **`config.https`**: if enabled the Pep-Proxy will listen only to the specified port. Note that the cert_file and keyfile are to be correctly configured, targeting either a self-signed certificate and file or the one generated by Certbot.

* **`config.account_host`**: the host where is reacheable the IdM (Horizon)

* **`config.app_host`** and config.app_port: host and port of the secured endpoint (in this case it is where is listening the Nginx endpoint that redirects to all the actual endpoints of the components.  

* **`config.app_ssl`** = true;  Use true if the app server listens in https

* **`config.username and config.password`**: they are the username and password provided by the IdM portal when registering the Pep-Proxy to the application.

**IMPORTANT NOTE**. In order to apply the configuration modified above, the PEP Proxy container **MUST BE** restarted, by running `docker CONTAINER_NAME restart` command
