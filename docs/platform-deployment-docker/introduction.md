---
id: introduction
title: Introduction
---

This project represents the reference platform implementation of the EU H2020 [SynchroniCity](https://synchronicity-iot.eu/) project.

In order to make the deployment as easy and quick as possible, the platform architecure was defined with a container deployment approach using [Docker compose](https://docs.docker.com/compose/).

[Link to gitlab project](https://gitlab.com/synchronicity-iot/platform-deployment-docker)

## Table of Contents
1.  [Platform architecture description](description)
2. [Installation and configuration](configuration)
3. [Security Configuration](security)
4. [Usage Scenarios examples and annotations](examples)
5. [Summary table of public instance endpoints](endpoints)
