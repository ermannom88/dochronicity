To run the code use the following commands:

- cd to the website folder

```
cd website
```
use either npm or yarn

###### - npm: 
- Install packages
```
npm install
```
- Run the code
```
npm start
```

###### - yarn:
- Install packages
```
yarn install
```
- Run the code
```
yarn start
```
